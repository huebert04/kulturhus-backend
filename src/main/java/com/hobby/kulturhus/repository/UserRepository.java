package com.hobby.kulturhus.repository;

import com.hobby.kulturhus.models.User;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
@ComponentScan(basePackages = "no.experis.kulturhusAPI.controllers, no.experis.kulturhusAPI.models")
public interface UserRepository extends JpaRepository<User, UUID> {
}
