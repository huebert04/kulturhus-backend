package com.hobby.kulturhus.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KulturHusController {

    @RequestMapping("/")
    public String index() {
        return "Greetings Earthlings.";
    }
}
