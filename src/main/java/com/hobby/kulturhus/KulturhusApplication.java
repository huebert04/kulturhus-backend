package com.hobby.kulturhus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KulturhusApplication {

	public static void main(String[] args) {
		SpringApplication.run(KulturhusApplication.class, args);
	}

}
